
(function () {
    'use strict';

    // Location Provider
    function configAngularMode($locationProvider, $resourceProvider) {

        // URL handling
        $locationProvider.html5Mode(true).hashPrefix('!');
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }

    configAngularMode.$inject =['$locationProvider', '$resourceProvider'];

    angular
        .module('bravoureAngularApp')
        .config(configAngularMode);

})();
